function ttimer(end_time) {
	// FF по другому воспринимает таймзоны
	var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

	function update() {
		var rem_time = new Date(end_time - (new Date()));
		//rem_time.setDate(rem_time.getDate()-1);
		rem_time.setHours(rem_time.getHours()-4);
		if(is_firefox){
			rem_time.setHours(rem_time.getHours()+1);
		}

		var rdays = (rem_time.getDate() - 1),
			rhours = rem_time.getHours(),
			rminutes = rem_time.getMinutes();

		if(rdays < 10) {
			rdays = '0' + rdays;
		}
		if(rminutes < 10) {
			rminutes = '0' + rminutes;
		}
		if(rhours < 10) {
			rhours = '0' + rhours;
		}

		document.getElementById('days-left').innerHTML = rdays;
		document.getElementById('hours-left').innerHTML = rhours;
		document.getElementById('minutes-left').innerHTML = rminutes;

		if(end_time - (new Date()) > 0){
			setTimeout(update, 5000);
		}
	}
	update();
}

var scrolling = false;
$(document).ready(function () {
	// запускаем таймер
	ttimer(new Date(2015, 9, 14, 12,0,0));
	
	// активные пункты меню
	$('#cd-top-nav ul a').click(function () {
		scrolling = true;
		$('#cd-top-nav ul a').removeClass('active-menu');
		$(this).addClass('active-menu');
		setTimeout(function () {
			scrolling = false;
		}, 2000);
	});
	
	$(document).scroll(function () {
		if(!scrolling) {
			$('#cd-top-nav ul a').removeClass('active-menu');
		}
	});
});
